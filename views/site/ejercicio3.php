<?php

use yii\helpers\Html;

echo Html::tag("h2","aprobados");

echo Html::ul($aprobados, [
    "item" => function ($dato) {

        return $this->render("_mostrar1", [
            "dato" => $dato
        ]);   //reutilizamos la subvista mostrar1
    }
]);
echo Html::tag("h2","suspensos");
echo Html::ul($suspensos, [
    "item" => function ($dato) {

        return $this->render("_mostrar1", [
            "dato" => $dato
        ]);   //reutilizamos la subvista mostrar1
    }
]);
