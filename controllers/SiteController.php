<?php

namespace app\controllers;

use app\models\Alumno;
use app\models\Alumnos;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;
use const YII_ENV_TEST;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    public function actionEjercicio1() {    //array de arrays asociativos
        $alumnos = [
            [
                "id" => 1,
                "nombre" => "EVA",
                "curso" => "Excel",
            ],
            [
                "id" => 2,
                "nombre" => "Luis",
                "curso" => "Excel",
            ],
            [
                "id" => 3,
                "nombre" => "Susana",
                "curso" => "Access",
            ],
        ];

        return $this->render('ejercicio1', [
                    "datos" => $alumnos
        ]);
    }

    
    //accion que permite mostrar un array enumerado de arrays asociativos utilizando html::ul, html::ol o foreach
    public function actionEjercicio2() {  //array unidimensional
        $numeros = [
            1, 2, 3, 4, 5, 6, 7
        ];

        return $this->render('ejercicio2', [
                    "datos" => $numeros
        ]);
    }

    public function actionEjercicio3() { //dos arrays asociatvos bidimensionales
        $aprobados = [
            [
                "id" => 1,
                "nombre" => "EVA",
                "curso" => "Excel",
            ],
            [
                "id" => 2,
                "nombre" => "Luis",
                "curso" => "Excel",
            ],
        ];

        $suspensos = [
            [
                "id" => 3,
                "nombre" => "Susana",
                "curso" => "Access",
            ],
        ];

        return $this->render('ejercicio3', [
                    "aprobados" => $aprobados,
                    "suspensos" => $suspensos
        ]);
    }

    public function actionEjercicio4() {  //array enumerado de arrays asociativos pero se lo paso al dataprovider usamos el widget gridview
        $alumnos = [
            [
                "id" => 1,
                "nombre" => "EVA",
                "curso" => "Excel",
            ],
            [
                "id" => 2,
                "nombre" => "Luis",
                "curso" => "Excel",
            ],
            [
                "id" => 3,
                "nombre" => "Susana",
                "curso" => "Access",
            ],
        ];

//        $alumno->id=1;
//        $alumno->nombre="EVA";
//     ]   $alumno->curso="excel";

              //convertimos $dataProvider en un proveedor de datos, es un objeto de la clase arraydataprovider, que recibe un array enumerado de arrays asocativos(si no no funciona)
        $dataProvider = new ArrayDataProvider([
            "allModels" => $alumnos
        ]);
        
        //mandamos el proveedor de datos a la vista, y utilizamos el gridview para visualizar los registros

        return $this->render("ejercicio4", [
                    "dataProvider" => $dataProvider
        ]);
    }
    // tengo un array de objetos de tipo modelo
    //para renderizar usando un Gridview
    public function actionEjercicio5() {
        //array de modelos

        $alumnos = [
            new Alumnos([
                "id" => 1,
                "nombre" => "EVA",
                "curso" => "excel"
                    ]),
            new Alumnos([
                "id" => 2,
                "nombre" => "luis",
                "curso" => "Access"
                    ]),
            new Alumnos([
                "id" => 3,
                "nombre" => "Antonio",
                "curso" => "Word"
                    ]),
        ];

        $dataProvider = new ArrayDataProvider([
            "allModels" => $alumnos
        ]);

        return $this->render("ejercicio4", [//en ejercicio 5 es igual que en la 4

                    "dataProvider" => $dataProvider
        ]);
    }
//tengo un array de modelos y lo visualizo con un
    public function actionEjercicio6() {  //aqui mostramos con listview
        $alumnos = [
            new Alumnos([
                "id" => 1,
                "nombre" => "EVA",
                "curso" => "excel"
                    ]),
            new Alumnos([
                "id" => 2,
                "nombre" => "luis",
                "curso" => "Access"
                    ]),
            new Alumnos([
                "id" => 3,
                "nombre" => "Antonio",
                "curso" => "Word"
                    ]),
        ];

        $dataProvider = new ArrayDataProvider([
            "allModels" => $alumnos
        ]);

        return $this->render("ejercicio6", [//en ejercicio 5 es igual que en la 4

                    "dataProvider" => $dataProvider
        ]);
    }

    /**
      } * Login action.
     *
     * @return Response|string
     */
    public function actionEjercicio7() {
        //quiero consultar la tabla alumno
        //consulta sin ejecutar
        //devuelve un ActiveQuery

        $query = Alumno::find();

        $dataProvider = new ActiveDataProvider([
            "query" => $query
        ]);

        //mandarle el resultado de la query 
        //a una vista

        return $this->render("ejercicio7", [
                    "dataProvider" => $dataProvider
        ]);
    }

    public function actionEjercicio8() {
        //quiero consultar la tabla alumno
        //envia un array
        //devuelve un ActiveQuery

        $resultado = Alumno::find()->all();    //si pongo all ya tengo la consultaejecutada y no devuelve un active query sino un array del activerecord
        
        
        $dataProvider=new ArrayDataProvider([
            
            "allModels"=>$resultado
            
        ]);
        
        
//        $resultado[0]->curso="exceL";
//        $resultado[0]->save();  //save hace un update.

//        $dataProvider = new ActiveDataProvider([
//            "query" => $query
//        ]);

        //mandarle el resultado de la query 
        //a una vista

       return $this->render("ejercicio7", [
                    "dataProvider" => $dataProvider
      ]);
    }
    
    public function actionEjercicio9(){
        // creo un modelo llamado Alumnos
        
        $model=new Alumnos();
        $model->id=10;
        $model->nombre="Federico";
        $model->curso="WORD";
        
        // mando el modelo a la vista
        //vamos a utilizar el visuualizador que falta DETAILVIEW, valido para un unico modelo que es el que tenemos ahora.
        
        return $this->render("ejercicio9",[
            
            "model"=>$model
            
        ]);
        
        
        
        
    }
    
    public function actionEjercicio10(){
        //vamos a crear un registro con un array asociativo
        
        $dato=[
            
            "id"=>10,
            "nombre"=>"federico",
            "curso"=>"Word"
            
        ];
        //mando el array asociativo a una vista que tiene el detailview
        return $this->render("ejercicio9",[
            
           "model"=>$dato,   
        ]);
    }
    
    public function actionEjercicio11(){
        //aqui necesitamos un activerecord
        
        $model=new Alumno(); //en singular es una clase que extiende activerecord, en plural es un model.
                
          $model->id=10;
        $model->nombre="Frenderico";
        $model->curso="WORDo";  
        
        //al ser activerecord puedo guardar y actualizar en la tabla con save
        
        $model->save();
        
        // mando el modelo a la vista
        //vamos a utilizar el visuualizador que falta DETAILVIEW, valido para un unico modelo que es el que tenemos ahora.
        
        return $this->render("ejercicio9",[
            
            "model"=>$model
                
            ]);
        
    }
    
     public function actionEjercicio12($id=1){
        //aqui vamos a extraer un registro de la tabla 
        // creando una clase basado en activerecord ejecutando la consluta sql 
         //select * from alumno where id=1
     
        
        $model=Alumno::findOne($id);
                
          
        
        //al ser activerecord puedo guardar y actualizar en la tabla con save
        
       
        
        // mando el modelo a la vista
        //vamos a utilizar el visuualizador que falta DETAILVIEW, valido para un unico modelo que es el que tenemos ahora.
        
        return $this->render("ejercicio9",[
            
            "model"=>$model
                
            ]);
     }
     
     
     public function actionEjercicio13(){
         
         //array con el contenido del directorio actual
         
         $contenido= scandir(".");
         
         
         $dataProvider= new ArrayDataProvider([
             
             "allModels"=>$contenido
         ]);
         
         return $this->render("ejercicio13",[
             
           "contenido"=>$contenido,
             "dataProvider"=>$dataProvider
         ]);
         
         
     }
}
