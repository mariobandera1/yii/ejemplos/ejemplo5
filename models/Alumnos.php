<?php

namespace app\models;

class Alumnos extends \yii\base\Model {
    
    public int $id;
    public string $nombre;
    public string $curso;
    
    
    public function rules(): array {
        
        return[
            [
                
                ["id",
                "nombre"
                ],       //las propiedades, si son mas de una, van dentro de otros corchetes, y luego va el validator
                
                "required"
                               
                
            ],
            
            
            [["nombre"],"string"],
            [["id"],"integer"],
            [["curso"],"safe"]
            
        ];
        
        
        
    
    }
    public function attributeLabels(): array {
    
    
    return [
        
        "id"=>" codigo del alumno ",
        "nombre"=>" Nombre : ",
        "curso"=>" Ultimo curso realizado :",
        
    ];
}
    
}
